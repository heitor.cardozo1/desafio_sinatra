# frozen_string_literal: true

# app.rb

require 'sinatra'

get '/' do
  @title = 'About Me'
  erb :index
end

get '/projects' do
  @title = 'Projects'
  erb :project
end

get '/education' do
  @title = 'Education'
  erb :education
end
