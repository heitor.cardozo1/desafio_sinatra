# frozen_string_literal: true

ENV['APP_ENV'] = 'test'

require_relative '../../app'
require 'test/unit'
require 'rack/test'

RSpec.describe 'Sinatra App' do
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  describe 'GET /' do
    context 'When access About Me page' do
      it 'Returns 200 status code' do
        get '/'
        expect(last_response.status).to be 200
      end
    end
  end

  describe 'GET /projects' do
    context 'When access Projects page' do
      it 'Returns 200 status code' do
        get '/'
        expect(last_response.status).to be 200
      end
    end
  end

  describe 'GET /education' do
    context 'When access Education page' do
      it 'Returns 200 status code' do
        get '/'
        expect(last_response.status).to be 200
      end
    end
  end

  describe 'GET missing route' do
    context 'When try to access unknown page' do
      it 'Returns 404 status code' do
        get '/unknown'
        expect(last_response.status).to be 404
      end
    end
  end
end
